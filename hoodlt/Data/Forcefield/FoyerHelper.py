"""
:module: FoyerHelper
:platform: Unix
:synopsis: Class to read values from a molecule structure file and generate a ForceFieldReader-compliant FF using Foyer

.. moduleauthor:: Arnau Jurado Romero <arnau.jurado@upc.edu> April 2024

..TODO: 
..                  - Handle hardcoded qualifiers in atoms, bonded and nonbonded potentials: this is tricky
..                  - Handle RB torsions vs periodic dihedrals
..                  - Special pairs are hardcoded to LJ and coulomb
"""
#Additional modules from default hoodlt: foyer,parmed,chardet (needed by parmed), rdkit (for sdf files)
import importlib_resources
import pandas as pd
import numpy as np
import foyer
import parmed
class FoyerHelper:
    """
    Reads molecule structure file in SDF format, generates a forcefield using 
    Foyer and creates a dictionary of pandas dataframes to be used by
    ForceFieldReader.
    """

    def __init__(self, mol_path, is_already_structure=False):
        """
        Creates the object

        :param mol_path: path to the structure file to read from
        """
        if(is_already_structure):
            #Here if we already have a parmed structure we directly assign it
            self.mol = mol_path
        else:
            #Here we load the molecule directly, parmed will take care of the rest
            self.mol = parmed.load_file(mol_path,structure=True)

    def apply_ff(self, ff_path,disable_assertions=False):
        """
        Apply FF from an .xml file to a parmed structure object using foyer
        """
        self.ff = foyer.Forcefield(ff_path)
        if(disable_assertions):
            self.mol_ff = self.ff.apply(self.mol,assert_bond_params=False,assert_angle_params=False,assert_dihedral_params=False,assert_improper_params=False)
        else:
            self.mol_ff = self.ff.apply(self.mol)

    def append_to_forcefield(self,ff):
        """
        Append Foyer data to already existing ForceFieldReader object
        """
        units = ff.get_units()
        DF = self.make_dataframes_from_ff(units)
        for attr in list(DF.keys()):
            ff.df_dict[attr] = pd.concat([ff.df_dict[attr],DF[attr]],ignore_index=True)

    def make_dataframes_from_ff(self,units):
        """
        Create a dictionary of dataframes with the different keys corresponding
        to a different category of the forcefield. See .xlsx files for 
        reference on each key's meaning
        """
        self.units = units
        group_df     = self.make_groups()
        nonbonded_df = self.make_nonbonded()
        bond_df      = self.make_bonds()
        angle_df     = self.make_angles()
        dihedral_df  = self.make_dihedrals()
        special_df   = self.make_specials()
        # print(group_df    )  
        # print(nonbonded_df) 
        # print(bond_df     )  
        # print(angle_df    )  
        # print(dihedral_df )  
        # print(special_df  ) 
        return {'groups':group_df,'nonbonded':nonbonded_df,'bond':bond_df,'angle':angle_df,'dihedral':dihedral_df,'special_pair':special_df}

    def make_groups(self):
        """
        Create a pandas df in the same structure as what would be read from a
        the 'groups' sheet of a hoodlt .xlsx forcefield.
        """
        mol_atom_df = self.mol_ff.to_dataframe().drop_duplicates('type')
        cols = ['name','qualifier','long name','molecular weight','charge']
        group_df = pd.DataFrame(columns=cols)
        for atype in range(mol_atom_df.shape[0]):
            name = mol_atom_df.iloc[atype]['type']
            mass = mol_atom_df.iloc[atype]['mass']
            charge = mol_atom_df.iloc[atype]['charge']
            newtype = pd.DataFrame({
                cols[0]:[name],
                cols[1]:['atom'], #Qualifier is hardcoded to atom
                cols[2]:[name], #Long name and name are the same
                cols[3]:[mass],
                cols[4]:[charge]
                }) 
            #Type casting avoids warning about matching dtypes with empty dataframe
            group_df = pd.concat([group_df.astype(newtype.dtypes),newtype],ignore_index=True)
        return group_df

    def make_nonbonded(self):
        """
        Create a pandas df in the same structure as what would be read from a
        the 'nonbonded' sheet of a hoodlt .xlsx forcefield.
        """
        mol_atom_df = self.mol_ff.to_dataframe().drop_duplicates('type')
        cols = ['name','qualifier','param1','param2','numparameters']
        nonbonded_df = pd.DataFrame(columns=cols)
        for atype in range(mol_atom_df.shape[0]):
            name = mol_atom_df.iloc[atype]['type']
            epsilon = mol_atom_df.iloc[atype]['epsilon']
            sigma = mol_atom_df.iloc[atype]['rmin']*2/2**(1/6) #For some reason foyer only set the rmin value in parmed
            if('Nm' in self.units.name): sigma /= 10. #Convert to nanometers if necessary, if not assume angstroms
            newtype = pd.DataFrame({
                cols[0]:[name],
                cols[1]:['LJ'], #Qualifier is hardcoded to LJ
                cols[2]:[sigma],
                cols[3]:[epsilon*4.184], #Foyer XML is in kcal/mol
                cols[4]:[2]
                }) 
            nonbonded_df = pd.concat([nonbonded_df.astype(newtype.dtypes),newtype],ignore_index=True)
        return nonbonded_df

    def make_bonds(self):
        """
        Create a pandas df in the same structure as what would be read from a
        the 'bond' sheet of a hoodlt .xlsx forcefield.
        """
        lbonds = self.mol_ff.bonds
        cols = ['name','qualifier','param1','param2','numparameters']
        bond_df = pd.DataFrame(columns=cols)
        #Add bonds to df, avoid repetitions
        addedtypes = []
        for bond in lbonds: #bond is a parmed topology object
            atype1 = bond.atom1.type #atom in bond is also a parmed topology object
            atype2 = bond.atom2.type
            k,r0 = bond.type.k,bond.type.req
            if('Nm' in self.units.name): k *= 100. #Convert to nanometers if necessary, if not assume angstroms
            if('Nm' in self.units.name): r0 /= 10. #Convert to nanometers if necessary, if not assume angstroms
            btype = atype1+'-'+atype2
            if not (btype in addedtypes): 
                newtype = pd.DataFrame({
                    cols[0]:[btype],
                    cols[1]:['harmonic'],
                    cols[2]:[k*4.184*2], #Foyer XML is in kcal/mol and nm and lacks the 1/2
                    cols[3]:[r0],
                    cols[4]:[2]
                    }) 
                bond_df = pd.concat([bond_df.astype(newtype.dtypes),newtype],ignore_index=True)
                addedtypes.append(btype)
        return bond_df

    def make_angles(self):
        """
        Create a pandas df in the same structure as what would be read from a
        the 'angle' sheet of a hoodlt .xlsx forcefield.
        """
        langles = self.mol_ff.angles
        cols = ['name','qualifier','param1','param2','numparameters']
        angle_df = pd.DataFrame(columns=cols)
        #Add bonds to df, avoid repetitions
        addedtypes = []
        for angle in langles: #bond is a parmed topology object
            atype1 = angle.atom1.type #atom in bond is also a parmed topology object
            atype2 = angle.atom2.type
            atype3 = angle.atom3.type
            k,t0 = angle.type.k,angle.type.theteq
            atype = atype1+'-'+atype2+'-'+atype3
            if not (atype in addedtypes): 
                newtype = pd.DataFrame({
                    cols[0]:[atype],
                    cols[1]:['harmonic'],
                    cols[2]:[k*4.184*2], #Foyer XML is in kcal/mol and doesn't have 1/2
                    cols[3]:[t0*np.pi/180.0], #Foyer XML is in degrees
                    cols[4]:[2]
                    }) 
                angle_df = pd.concat([angle_df.astype(newtype.dtypes),newtype],ignore_index=True)
                addedtypes.append(atype)
        return angle_df

    def make_dihedrals(self):
        """
        Create a pandas df in the same structure as what would be read from a
        the 'dihedral' sheet of a hoodlt .xlsx forcefield.
        """
        ldihedrals = self.mol_ff.rb_torsions
        cols = ['name','qualifier','param1','param2','param3','param4','numparameters']
        dihedral_df = pd.DataFrame(columns=cols)
        #Add bonds to df, avoid repetitions
        addedtypes = []
        for dihedral in ldihedrals: #bond is a parmed topology object
            atype1 = dihedral.atom1.type #atom in bond is also a parmed topology object
            atype2 = dihedral.atom2.type
            atype3 = dihedral.atom3.type
            atype4 = dihedral.atom4.type
            c0,c1,c2,c3,c4 = dihedral.type.c0,dihedral.type.c1,dihedral.type.c2,dihedral.type.c3,dihedral.type.c4
            dtype = atype1+'-'+atype2+'-'+atype3+'-'+atype4

            #Foyer uses fourier series dihedrals (Ryckaert-Belleman) while HOOMD 
            #uses OPLS-like dihedrals so we have to convert between them
            k1 = -2*c1 -3*c3/2
            k2 = -c2 -c4
            k3 = -c3/2
            k4 = -c4/4
            if not (dtype in addedtypes): 
                newtype = pd.DataFrame({
                    cols[0]:[dtype],
                    cols[1]:['opls'],
                    cols[2]:[k1*4.184], #They are also in kcal/mol
                    cols[3]:[k2*4.184],
                    cols[4]:[k3*4.184],
                    cols[5]:[k4*4.184],
                    cols[6]:[4]
                    }) 
                dihedral_df = pd.concat([dihedral_df.astype(newtype.dtypes),newtype],ignore_index=True)
                addedtypes.append(dtype)
        return dihedral_df

    def make_specials(self):
        """
        Create a pandas df in the same structure as what would be read from a
        the 'dihedral' sheet of a hoodlt .xlsx forcefield.
        """
        ldihedrals = self.mol_ff.rb_torsions
        cols = ['name','qualifier','param1','param2','numparameters']
        special_df = pd.DataFrame(columns=cols)
        #Add bonds to df, avoid repetitions
        addedtypes = []
        for dihedral in ldihedrals: #bond is a parmed topology object
            atype1 = dihedral.atom1.type #atom in bond is also a parmed topology object
            atype2 = dihedral.atom2.type
            atype3 = dihedral.atom3.type
            atype4 = dihedral.atom4.type
            dtype = atype1+'-'+atype2+'-'+atype3+'-'+atype4
            if not (dtype in addedtypes): 
                newtype = pd.DataFrame({
                    cols[0]:[dtype],
                    cols[1]:['LJ'],
                    cols[2]:['LJ'],
                    cols[3]:[self.ff.lj14scale],
                    cols[4]:[2]
                    }) 
                special_df = pd.concat([special_df.astype(newtype.dtypes),newtype],ignore_index=True)
                addedtypes.append(dtype)
        addedtypes = []
        for dihedral in ldihedrals: #bond is a parmed topology object
            atype1 = dihedral.atom1.type #atom in bond is also a parmed topology object
            atype2 = dihedral.atom2.type
            atype3 = dihedral.atom3.type
            atype4 = dihedral.atom4.type
            dtype = atype1+'-'+atype2+'-'+atype3+'-'+atype4
            if not (dtype in addedtypes): 
                newtype = pd.DataFrame({
                    cols[0]:[dtype],
                    cols[1]:['Coulomb'],
                    cols[2]:[self.ff.coulomb14scale],
                    cols[3]:['NaN'],
                    cols[4]:[1]
                    }) 
                # special_df = pd.concat([special_df.astype(newtype.dtypes),newtype],ignore_index=True)
                special_df = pd.concat([special_df,newtype],ignore_index=True)
                addedtypes.append(dtype)
        return special_df
    
    def get_combining_rule(self):
        return self.ff.combining_rule