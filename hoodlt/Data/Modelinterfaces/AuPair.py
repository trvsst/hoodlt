"""
:module: Au Pair
:platform: Unix, Windows
:synopsis: Defines the class to define a pair of Au interfaces that belong to the same rigid body

.. moduleauthor:: Arnau Jurado Romero <arnau.jurado@upc.edu>, June 2024
.. history:
"""

from __future__ import division

import numpy as np
from hoodlt.Data.Modelinterfaces.IntAbs import IntAbs


class AuPair(IntAbs):
    """
    Pair of planar interfaces made of gold atoms
    """

    def __init__(self, ff, d=2.8, repeats=[3,3], molecular_area=70.56, plane_dist=10.0, graf_pos=None):
        """

        :param ff: ForceFieldReaderObject
        :param d: interatomic distance (i.e diameter) in Angstroms
        :param repeats: number of repeats in each coordinate of the plane (needs to be odd so center is at 0,0)
        :param molecular_area: molecular area of each ligand in Angstroms^2
        :param plane_dist: distance between the 
        :param graft_pos: relative distance of the grafting sites to the plane, in Angstroms, if not given
                          defaults to d
        """

        if(graf_pos is None): graf_pos = d
        plane_atom_num = repeats[0]*repeats[1]
        self.num_particles = 2*plane_atom_num+1
        super(AuPair, self).__init__(ff, self.num_particles, 'AuPlane')


        self.position = np.zeros((self.num_particles,3)) #The first particle is at 0.0,0.0,0.0
        self.position[1:plane_atom_num+1,:2] = self.sc_lattice(d,repeats) #x and y coordinates
        self.position[plane_atom_num+1:,:2] = self.sc_lattice(d,repeats) #x and y coordinates
        self.position[1:plane_atom_num+1,2]  = -plane_dist/2 #z coordinate of first plane
        self.position[plane_atom_num+1:,2]  =  +plane_dist/2
        self.position = self.position*self.ff_reader.get_units().angstrom_to_construction

        self.mass[1:] = self.ff_reader.get_molecular_weight('Au')
        self.mass[0] = np.sum(self.mass[1:])

        # Types
        self.types = ['_Au', 'Au']
        self.typeid = ['_Au'] + ['Au']*(self.num_particles-1)

        lig_d = np.sqrt(molecular_area)
        lig_rep = [int((d*repeats[0])/lig_d),int((d*repeats[1])/lig_d)]

        plane_lig_num = lig_rep[0]*lig_rep[1]
        self.graft_num = 2*plane_lig_num
        self.graft_sites = np.zeros((self.graft_num,3))
        self.graft_sites[:plane_lig_num,:2] = self.sc_lattice(lig_d,lig_rep)
        self.graft_sites[plane_lig_num:,:2] = self.sc_lattice(lig_d,lig_rep)
        self.graft_sites[:plane_lig_num,2] = -plane_dist/2 - graf_pos
        self.graft_sites[plane_lig_num:,2] = +plane_dist/2 + graf_pos
        self.graft_sites = self.graft_sites*self.ff_reader.get_units().angstrom_to_construction

        # self.graft_sites = np.array([[0, 0, d]])
        # self.graft_num = 1

        #This needs to be changed
        self.moment_inertia[0] = np.array([3151.46512, 3151.46512, 3151.46512])

    def get_vector(self):
        """
        See Documentation in BasicSystemEntity
        """
        #Needs to be updated
        return self.position[2] - self.position[1]  # points along the z-axis
    

    @staticmethod
    def sc_lattice(d,repeats):
        """
        Calculates coordinates of sc lattice

        :param d: distance between lattice points
        :param repeats: repetitions in each direction
        :return: 
        """

        coordinates = np.zeros((repeats[0]*repeats[1],2))
        count = 0
        for ix in range(repeats[0]):
            for iy in range(repeats[1]):
                coordinates[count,0] = -d*(repeats[0]-1)/2 + d*ix
                coordinates[count,1] = -d*(repeats[1]-1)/2 + d*iy
                count += 1
        return coordinates
