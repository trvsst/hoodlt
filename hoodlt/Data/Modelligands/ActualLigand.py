"""
:module: ActualLigand
:platform: Unix, Windows
:synopsis: Implements a class defining a ligand defined in a file class

.. moduleauthor:: Alex Travesset <trvsst@ameslab.gov>
..                  - created class
..                Arnau Jurado Romero <arnau.jurado@upc.edu> May 2024
..                  - Ligand construction via prototype file
..                  - Automated FF building using parmed and foyer, see FoyerHelper
..                Arnau Jurado Romero <arnau.jurado@upc.edu> July 2024
..                  - Adapted the code to new way to handle force field objects
"""

import importlib_resources
import numpy as np
import parmed
from hoodlt.Data.Modelligands.LigandAbs import LigandAbs
from hoodlt.Data.Forcefield.FoyerHelper import FoyerHelper
import hoodlt.Data.Forcefield
import os


class AnyLigand(LigandAbs):
    """
    Defines a general ligand to be read from force field
    """

    def __init__(self, file_name, repeats, ff, ffxml, load_from_prototype=True, disable_assertions=False):
        """

        :param file_name: file name defining the ligand to be read by FoyerHelper
        :param repeats: number of repeats on the chain, only used if loading from prototype
        :param ff: ForceFieldReader object to which the new coefficients will be appended
        :param ffxml: xml file (with .xml) with the atomtyping information used by Foyer, see Foyer documentations
        :param load_from_prototype: use 'prototype' molecules to build the molecule
        :param disable_assertions: Foyer will continue even is all bonds, angles and dihedrals 
        are not defined
        """
        self.units = ff.get_units()
        if(load_from_prototype):
            mol = self.load_prototype_file(file_name,repeats,ff)
            FH = FoyerHelper(mol,is_already_structure=True)
        else:
            FH = FoyerHelper(file_name)
            super(AnyLigand, self).__init__(repeats, ff, len(FH.mol.atoms), file_name.split('.')[-2].strip())
            self.position = FH.mol.coordinates #Assign coordinates
            if('Nm' in self.units.name): self.position /= 10.0 #Adjust for nanometers

        
        rsc_path = importlib_resources.files(hoodlt.Data.Forcefield).joinpath(ffxml)
        #Check first if xml is in 
        if os.path.isfile(rsc_path):
            ffxml = rsc_path
        elif os.path.isfile(ffxml):
            pass
        else:
            raise FileNotFoundError('XML file not found in Data/Forcefield or on provided path')

        FH.apply_ff(ffxml, disable_assertions)
        self.DF = FH.make_dataframes_from_ff(self.units)
        FH.append_to_forcefield(self.ff_reader)
        self.ligand_ff = FH.mol_ff

        self.types = list(self.DF['groups']['name'])
        self.mass = np.zeros(self.num_particles)
        self.charge = np.zeros(self.num_particles)
        self.typeid  = []
        for a,atom in enumerate(self.ligand_ff):
            self.mass[a] = atom.mass
            self.charge[a] = atom.charge
            self.typeid.append(atom.type)

        self.diameter = np.zeros(self.num_particles) + 0.08
        self.body = self.num_particles*[-1]

    def load_prototype_file(self,file_name,repeats,ff):
        with open(file_name,'r') as f:
            name = f.readline().strip() #First line defines name
            #Read grafted group
            self.start_length = int(f.readline().strip())
            self.start_element = np.zeros([self.start_length],dtype=np.int32)
            self.start_position = np.zeros([self.start_length,3])
            f.readline() #Discard comment line
            for a in range(self.start_length):
                l = f.readline().strip().split()
                self.start_element[a] = int(l[0])
                self.start_position[a,:] = float(l[1]),float(l[2]),float(l[3])
            l = f.readline().strip().split() #Read start separation vector
            self.start_sepv = np.array([float(l[0]),float(l[1]),float(l[2])])

            #Read repeating group
            self.repeat_length = int(f.readline().strip())
            self.repeat_element = np.zeros([self.repeat_length],dtype=np.int32)
            self.repeat_position = np.zeros([self.repeat_length,3])
            f.readline() #Discard comment line
            for a in range(self.repeat_length):
                l = f.readline().strip().split()
                self.repeat_element[a] = int(l[0])
                self.repeat_position[a,:] = float(l[1]),float(l[2]),float(l[3])
            l = f.readline().strip().split() #Read repeat separation vector
            self.repeat_sepv = np.array([float(l[0]),float(l[1]),float(l[2])])

            #Read end group
            self.end_length = int(f.readline().strip())
            self.end_element = np.zeros([self.end_length],dtype=np.int32)
            self.end_position = np.zeros([self.end_length,3])
            f.readline() #Discard comment line
            for a in range(self.end_length):
                l = f.readline().strip().split()
                self.end_element[a] = int(l[0])
                self.end_position[a,:] = float(l[1]),float(l[2]),float(l[3])

        num_particles = self.start_length + repeats*self.repeat_length + self.end_length

        super(AnyLigand, self).__init__(repeats, ff, num_particles, name)
        #Element and particle positions
        self.element,self.position = self.build_chain()
        
        #Initialize parmed structure
        mol = parmed.Structure()
        #Add atoms to parmed structure
        for a in range(self.num_particles):
            mol.add_atom(parmed.Atom(atomic_number=int(self.element[a])),resname='A',resnum=0)
        mol.coordinates = self.position
        mol.assign_bonds() #Let parmed infer bonds from distances
        return mol

    def build_chain(self):
        """
        Called by initializer to get the positions
        
        :return: Elements and positions of all atoms
        """ 
        ele = np.zeros([self.num_particles],dtype=np.int32)
        xyz = np.zeros([self.num_particles,3])

        #Define grafting group positions
        ele[:self.start_length] = self.start_element
        xyz[:self.start_length] = self.start_position
        #Define repeating group positions
        for repeat in range(self.repeats):
            for a in range(self.repeat_length):
                ele[self.start_length + repeat*self.repeat_length + a] = self.repeat_element[a]
                if(repeat==0): #If first repetition insert as-is to the last atom of start + separation vector
                    xyz[self.start_length + a,:] = xyz[self.start_length-1,:] + self.start_sepv + self.repeat_position[a,:]
                else: #Else add the position of last atom of the previous repeat + separation vector + relative position within repeat unit
                    xyz[self.start_length + repeat*self.repeat_length + a,:] = xyz[last_atom_of_previous_repeat,:] + self.repeat_sepv + self.repeat_position[a,:]
            last_atom_of_previous_repeat = self.start_length + repeat*self.repeat_length + self.repeat_length-1
        #Define end group positions
        ele[self.start_length+self.repeats*self.repeat_length:self.num_particles] = self.end_element
        xyz[self.start_length+self.repeats*self.repeat_length:self.num_particles,:] = xyz[self.start_length+self.repeats*self.repeat_length-1,:] + self.end_position

        for a in range(self.num_particles):
            print(ele[a],xyz[a,0],xyz[a,1],xyz[a,2])

        return ele,xyz


    def get_vector(self):
        """
        See documentation in BasicSystemEntity
        """

        return self.position[-1] - self.position[0]

    def get_bonds(self):
        """
        override of method in SolventAbs
        returns a list of the particle indices (2 particles per bond) involved in the bonds in the system
        
        :return: a shape (Nbonds,2) numpy array
        """
        b_list = []
        for bond in self.ligand_ff.bonds:
            b_list.append([bond.atom1.idx,bond.atom2.idx])
        return np.array(b_list, dtype=int)

    def get_bond_types(self):
        """
        override of the method in SolventAbs
        used to dump_gsd
        
        :return: list of all the bond types in the system
        """
        b_types = []
        for bond in self.ligand_ff.bonds:
            btype = bond.atom1.type+'-'+bond.atom2.type
            b_types.append(btype)
        return b_types

    def get_angles(self):
        """
        override of method in SolventAbs
        used to dump_gsd
        
        :return: a shape (Nangles,3) numpy array
        """
        a_list = []
        for angle in self.ligand_ff.angles:
            a_list.append([angle.atom1.idx,angle.atom2.idx,angle.atom3.idx])
        return np.array(a_list, dtype=int)

    def get_angle_types(self):
        """
        override of the method in SolventAbs
        used to dump_gsd
        
        :return: list of all the angle types in the system
        """
        a_types = []
        for angle in self.ligand_ff.angles:
            atype = angle.atom1.type+'-'+angle.atom2.type+'-'+angle.atom3.type
            a_types.append(atype)
        return a_types

    def get_dihedrals(self):
        """
        override of method in SolventAbs
        used to dump_gsd
        
        :return: a list of particle indices (4 particles per dihedral) involved in the dihedrals in the system
        """
        d_list = []
        for dihedral in self.ligand_ff.rb_torsions:
            d_list.append([dihedral.atom1.idx,dihedral.atom2.idx,dihedral.atom3.idx,dihedral.atom4.idx])
        return np.array(d_list, dtype=int)

    def get_dihedral_types(self):
        """
        override of the method in SolventAbs
        used to dump_gsd

        :return: list of all the dihedral types in the system
        """
        d_types = []
        for dihedral in self.ligand_ff.rb_torsions:
            btype = dihedral.atom1.type+'-'+dihedral.atom2.type+'-'+dihedral.atom3.type+'-'+dihedral.atom4.type
            d_types.append(btype)
        return d_types

    def get_pairs(self):
        """
        override of method in SolventAbs
        used to dump_gsd
        
        :return: a array of 1-4 pairs (particle indices of first and fourth particle for all dihedrals)
        """
        s_list = []
        for dihedral in self.ligand_ff.rb_torsions:
            s_list.append([dihedral.atom1.idx,dihedral.atom4.idx])
        return np.array(s_list, dtype=int)

    def get_pair_types(self):
        """
        override of the method in SolventAbs
        used to dump_gsd

        :return: list of all the dihedral types in the system
        """
        return self.get_dihedral_types()

    def get_constraints(self):
        """
        override of method in SolventAbs
        returns a list of the particle indices (2 particles per constraint) involved in the constraints in the system
        
        :return: a shape (Nrigidbonds,2) numpy array
        """
        b_list = []
        for bond in self.ligand_ff.bonds:
            if(bond.atom1.element==1 or bond.atom2.element==1): #Default: bonds with H are rigid
                b_list.append([bond.atom1.idx,bond.atom2.idx])
        return np.array(b_list, dtype=int)

    def get_constraint_types(self):
        """
        override of the method in SolventAbs
        used to dump_gsd
        
        :return: list of all the constraint types in the system
        """
        b_types = []
        b_r0 = []
        for bond in self.ligand_ff.bonds:
            if(bond.atom1.element==1 or bond.atom2.element==1): #Default: bonds with H are rigid
                btype = bond.atom1.type+'-'+bond.atom2.type
                b_types.append(btype)
                req = bond.type.req
                if('Nm' in self.units.name): req /= 10.0 #If nanometers convert req
                b_r0.append(req)
        return b_r0
        # return b_types
