"""
:module: PointLigand
:platform: Unix, Windows
:synopsis: Implements a class defining a ligand that is a single atom. The name, LJ coefficients,
            charge and mass of the atom can be defined at runtime and will be added
            automatically to the force field passed to the constructor.

.. moduleauthor:: Arnau Jurado Romero <arnau.jurado@upc.edu> July 2024
.. history:
"""

import numpy as np
import pandas as pd
from hoodlt.Data.Modelligands.LigandAbs import LigandAbs

class PointLigand(LigandAbs):
    """
    Defines a ligand to be read from force field that is a single atom. The name, LJ coefficients,
    charge and mass of the atom can be defined at runtime and will be added
    automatically to the force field passed to the constructor.
    """

    def __init__(self, ff, name='L', mass=1.0, charge=0.0, epsilon=0.0, sigma=0.0):
        """

        :param ff: ForceFieldReader object to which the new coefficients will be appended
        :param name: name of the atom type of the ligand
        :param mass: mass (i.e. molecular weight) of the atom, in construction units
        :param charge: charge of the atom, in construction units
        :param epsilon: epsilon of the atom, in construction units
        :param sigma: sigma of the atom, in construction units

        """

        super(PointLigand, self).__init__(1, ff, 1, name)


        self.diameter = np.zeros(self.num_particles) + 0.08
        self.types = [name]
        self.typeid = [name]

        self.mass = np.array([mass])
        self.charge = np.array([charge])
        self.body = self.num_particles*[-1]

        self.position = np.zeros([1, 3])

        #Add groups to ForceFieldReader
        cols = ['name','qualifier','long name','molecular weight','charge']
        group_df = pd.DataFrame({
                cols[0]:[name],
                cols[1]:['atom'], #Qualifier is hardcoded to atom
                cols[2]:['Point ligand '+name],
                cols[3]:[mass],
                cols[4]:[charge]
                }) 
        ff.df_dict['groups'] = pd.concat([ff.df_dict['groups'],group_df],ignore_index=True)

        cols = ['name','qualifier','param1','param2','numparameters']
        nonbonded_df = pd.DataFrame({
                cols[0]:[name],
                cols[1]:['LJ'], #Qualifier is hardcoded to LJ
                cols[2]:[sigma],
                cols[3]:[epsilon*4.184], #Foyer XML is in kcal/mol
                cols[4]:[2]
                }) 
        ff.df_dict['nonbonded'] = pd.concat([ff.df_dict['nonbonded'],nonbonded_df],ignore_index=True)

    def get_vector(self):
        """
        See documentation in BasicSystemEntity
        """

        return np.array([1.0,0.0,0.0])
    
    def get_bonds(self):
        """
        override of method in SolventAbs
        returns a list of the particle indices (2 particles per bond) involved in the bonds in the system
        
        :return: a (self.repeats - 1)x2 numpy array
        """

        b_list = []

        return np.array(b_list, dtype=int)

    def get_bond_types(self):
        """
        override of the method in SolventAbs
        used to dump_gsd
        
        :return: list of all the bond types in the system
        """
        b_types = []

        return b_types

    def get_angles(self):
        """
        override of method in SolventAbs
        used to dump_gsd
        
        :return: a list of particle indices (3 particles per angle) involved in the angles in the system
        """

        a_list = []

        return np.array(a_list, dtype=int)

    def get_angle_types(self):
        """
        override of the method in SolventAbs
        used to dump_gsd
        
        :return: list of all the angle types in the system
        """

        a_types = []

        return a_types

    def get_dihedrals(self):
        """
        override of method in SolventAbs
        used to dump_gsd
        
        :return: a list of particle indices (4 particles per dihedral) involved in the dihedrals in the system
        """
        d_list = []


        return np.array(d_list, dtype=int)

    def get_dihedral_types(self):
        """
        override of the method in SolventAbs
        used to dump_gsd

        :return: list of all the dihedral types in the system
        """

        d_types = []

        return d_types

    def get_pairs(self):
        """
        override of method in SolventAbs
        used to dump_gsd
        
        :return: a array of 1-4 pairs (particle indices of first and fourth particle for all dihedrals)
        """

        s_list = []

        return np.array(s_list, dtype=int)

    def get_pair_types(self):
        """
        override of the method in SolventAbs
        used to dump_gsd

        :return: list of all the dihedral types in the system
        """

        return self.get_dihedral_types()

    def get_constraints(self):
        """
        override of method in SolventAbs
        returns a list of the particle indices (2 particles per constraint) involved in the constraints in the system
        
        :return: a (self.repeats - 1)x2 numpy array
        """

        c_list = []

        return np.array(c_list, dtype=int)

    def get_constraint_types(self):
        """
        override of the method in SolventAbs
        used to dump_gsd
        
        :return: list of all the constraint types in the system
        """

        return []