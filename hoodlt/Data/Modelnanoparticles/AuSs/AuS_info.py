"""
:module: TO_info
:platform: Unix, Windows
:synopsis: Calculates the radius of the given truncated octahedron (TO),
gives a list of available TO's within a range of radius

.. moduleauthor:: Xun Zha <xzha@iastate.edu> November 2018
.. history:
..                Alex Travesset <trvsst@ameslab.gov> June 2024
..                  - simplified the the class and made it compatible with the current hoodlt version
"""


import numpy as np
import numpy.linalg as la
import importlib_resources
import pandas as pd
from scipy.spatial import ConvexHull
import hoodlt.Data.Modelnanoparticles.TOs
import hoodlt.Data.Modelnanoparticles.AuSs


class AuSConfigInfo(object):
    def __init__(self, num_pnts, version):
        """

        :param num_pnts: number of atoms in the TO configuration
        :param version: degeneracy
        """

        # lattice constant
        filename = importlib_resources.files(hoodlt.Data.Modelnanoparticles.TOs)
        l_data = pd.read_csv(filename.joinpath('fcc_lattice_constant.csv'))
        l_constant = l_data[l_data.loc[:, 'atom'] == 'Au']['lattice constant'].values[0]
        self.a_nn = l_constant/np.sqrt(2)  # nearest neighbor distance
        self.v_ws = l_constant**3/4.0  # volume of Wigner-Seitz cell

        # filename containing TO information
        fl_name = 'N' + str(int(num_pnts)) + '_v' + str(int(version)) + '.txt'
        fl_path = filename.joinpath(fl_name)

        # read atom coordinates
        pos = np.genfromtxt(fl_path)  # all atom coordinates
        mat = la.norm(pos[np.newaxis, :, :]-pos[:, np.newaxis, :], axis=2)
        n_list = np.sum(np.logical_and(mat > 0.01, mat < 1.01), axis=1)
        # compute the number of atoms for each coordination number
        n_coord_number = [sum(n_list == j) for j in range(13)]

        # list of coordination number for each atom
        # pos_surf = pos[n_list < 12]  # coordinates of surface atoms
        # assign a volume to each atom according to coordination/12 \times v_ws
        volume = self.v_ws*np.sum(np.array(n_coord_number)*np.arange(13))/12.0
        # radius from volume
        radius_v = np.cbrt(volume*3/4./np.pi)
        diameter_v = 2*radius_v

        # convex hull area
        cnvx = ConvexHull(pos)
        area_c = cnvx.area*self.a_nn**2
        # radius from surface area
        radius_a = np.sqrt(area_c/(4.*np.pi))
        diameter_a = 2*radius_a

        # filename containing AuSs information
        filename = importlib_resources.files(hoodlt.Data.Modelnanoparticles.AuSs)
        fl_name = str(int(num_pnts)) + '_' + str(int(version)) + '_AuS.txt'
        fl_path = filename.joinpath(fl_name)

        # read atom coordinates
        pos_all = np.genfromtxt(fl_path)  # all atom coordinates
        num_core = np.sum(pos_all[:, 0] == 0)
        num_graft = np.sum(pos_all[:, 0] == 1)

        pos = np.zeros([num_graft, 3])
        pos[:, :] = pos_all[num_core:, 1:]
        # convex hull area
        cnvx = ConvexHull(pos)
        area_g = cnvx.area
        # radius from surface area
        radius_g = np.sqrt(area_g / (4. * np.pi))
        diameter_g = 2 * radius_g

        self.nc_geometry = {'core volume': volume, 'radius from core volume': radius_v,
                            'diameter from core volume': diameter_v, 'core area': area_g,
                            'core radius from core area': radius_a, 'core diameter from core area': diameter_a,
                            'nc area': area_g, 'nc radius from area': radius_g, 'nc diameter from area': diameter_g}
