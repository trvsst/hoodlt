"""
:module: NanoAbs
:platform: Unix, Windows
:synopsis: Defines the abstract classes used to store nanoparticles

.. moduleauthor:: Alex Travesset <trvsst@ameslab.gov>, March 2017
.. history:
..                Tommy Waltmann <tomwalt@iastate.edu> July 2019
..                  - updated grafting algorithm and removed some code we don't need anymore
..                  - orienting and de-orienting cores now updates the grafting sites as well
..                  - cores now have a list of graft sites separate from their positions list
..                  - moved many methods to analysis
..                Tommy Waltmann <tomwalt@iastate.edu> June 2019
..                  - Documentation
..                  - added get_vector for use in aligning cores
..                  - removed references to body_count variable when dumping
..                  - removed the '_' in front of many of the names of the methods
..                  - core_name is now set in basic entity class
..                Tommy Waltmann <tomwalt@iastate.edu> May 2019
..                  - made class inherit from BasicSystemEntity, moved many methods to that class
..                  - removed uncessary values stored by the constructor
..                  - rewrote many methods to be simpler, easier to understand
..                Alex Travesset <trvsst@ameslab.gov> June 2022
..                  - included rotations consistent with v3 (eliminated old orient)
..                Alex Travesset <trvsst@ameslab.gov> June 2024
..                  - cleaned up the class
..                  - added new member functions
"""

import numpy as np
import numpy.linalg as la
from scipy.spatial import ConvexHull
from hoodlt.Data.Modelconfigurations.BasicSystemEntity import BasicSystemEntity
from hoodlt.Groups.Quat import Quat


class NanoAbs(BasicSystemEntity):
    """
    Defines general nanoparticle cores
    """

    def __init__(self, forcefield, num_particles, name):
        """

        :param forcefield: the name of the forcefield to be used to construct this object
        :param num_particles: the number of particles (including rigid center) on this core
        :param name: the name of the core
        """

        super(NanoAbs, self).__init__(forcefield, num_particles, name)

        # grafter stuff
        self.graft_sites = np.array([])
        self.graft_num = None

        self.body = np.zeros(self.num_particles)  # each core will be one rigid body
        self.params_outside_forcefield = {}

    def volume(self):
        """Computes the nanoparticle volume

        :return:
        :rtype: float
        """

        raise ValueError("volume() method in NanoAbs is not implemented")

    def area(self):
        """returns the area using convex hull, may give somewhat misleading results if the grafters are not
        homogeneously dispersed

        :return: value of the area
        :rtype: float
        """

        cnvx = ConvexHull(self.graft_sites)
        return cnvx.area

    def grafting_density(self):
        """
        Returns the grafting density

        :return: grafting density
        """

        return self.graft_num/(np.pi*self.nc_diameter())

    def nc_diameter(self):
        """Returns the core diameter

        :return: diameter
        :rtype: float
        """

        pos = self.position[0]
        return 2*np.average(la.norm(self.graft_sites - pos, axis=1))

    def rotate(self, quat):
        """
        rotates the core as well as its grafting sites
        (this is used in preparing simulations)

        :param quat: quaternion as a 4-tuple
        :return:
        """
        qt = Quat()
        quatp = self.orientation[0]

        self.orientation[0] = tuple(qt.multiply(quat, quatp))

    def rotate_actual(self, quat):
        """
        rotates the core as well as its grafting sites and implements the rotation
        (this is used to draw actual rotated nanoparticles)

        :param quat: quaternion as a 4-tuple
        :return:
        """
        qt = Quat()
        relative_center = self.position[0]
        for ind in range(len(self.position)):
            self.position[ind] = relative_center + qt.rotation(quat, self.position[ind]-relative_center)

    def align_core(self, vector):
        """
        Aligns the core's instrinsic vector with the input vector, using the rigid center as the origin for the rotation

        :param vector: the vector to align the core's intrinsic vector with
        :return: None
        """

        self.align(self.position[0], vector)

    def shift(self, vector):
        """
        Shift the core by the given vector

        :param vector: the vector to shift the core by
        :return:
        """

        super(NanoAbs, self).shift(vector)

        # shift the graft sites as well
        self.graft_sites = self.graft_sites[:] + vector

    def align_core_using_vectors(self, original_vector, vector_to_rotate_to):
        """
        Rotates the core in the exact same way that original_vector must be rotated so that it points in the same
        direction as vector_to_rotate_to
        :param original_vector: vector that will be used as the start reference for the rotation of the entity
        :param vector_to_rotate_to: vector that will serve as end reference for the rotation of the entity
        :return: None
        """

        self.align_using_vectors(self.position[0], original_vector, vector_to_rotate_to)

    def get_name(self):
        """
        name

        :returns: name
        """

        return self.name

    def parameters_outside_force_field(self):
        """
        parameters defined in the core that do no follow from the force field

        """

        return self.params_outside_forcefield
