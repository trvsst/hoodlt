"""
:module: TO
:platform: Unix, Windows
:synopsis: Defines the class for truncated octahedra

.. moduleauthor:: Xun Zha <xzha@iastate.edu> November 2018
.. history:
..                Tommy Waltmann <tomwalt@iastate.edu> June 2019
..                  - rewrote class so it is more general
..                  - reworked class so it fits with BasicSystemEntity
..                  - name is now set in basic entity class
..
..                Alex Travesset <trvsst@ameslab.gov> June 2024
..                  - Adapted the class to the current hoodlt structure
..                Arnau Jurado Romero <arnau.jurado@upc.edu> July 2024
..                  - Now accepts FF object direclty 
"""
import numpy as np
import numpy.linalg as la

from hoodlt.Data.Modelnanoparticles.CoreWithPositionsInFile import CoreWithPositionsInFile
from hoodlt.Data.Modelnanoparticles.TOs.TO_info import TOInfo

class TO(CoreWithPositionsInFile):
    """
    Construction of a nc system with grafters

    Grafters are provided as integers and are placed at a distance given by the diameter :math:`\\sigma` as obtained
        from the actual force-field.

    """

    def __init__(self, ff, num_core_particles, version, atom_core='Au', scale=None,
                 name_rigid_center=None, graft_index_positions=None, graft_atom=None):
        """

        :param ff: ForceFieldReader object to be used to construct this object
        :param num_core_particles: number of particles on the shell of the core
        :param version: degeneracy
        :param atom_core: type of the core particles list should either be length 1 or length num_core_particles
        :param scale: factor by which to scale positions. Default, fcc nearest neighbor distances
        :param name_rigid_center: name of the rigid center that will be used for this core, without the '_' prefix
        :params graft_index_positions: positions of the grafting atoms, as integers referring to the actual nc positions
        :params graft_atom: grafting atoms
        """

        # read configuration information
        info = TOInfo(num_core_particles, version, atom_core)
        self.a_nn = info.a_nn
        dict_nc = info.nc_geometry
        self.radius = dict_nc['core radius from core area']
        self.area = dict_nc['core area']
        self.volume = dict_nc['core volume']

        # determine how things will be scaled
        if scale is None:
            factor = self.a_nn
        else:
            factor = scale

        # make the file name
        name = 'N'+str(int(num_core_particles))+'_v'+str(int(version))+'.txt'
        file_name = "TOs/" + name

        # call super
        super(TO, self).__init__(ff, num_core_particles, file_name, [atom_core], factor, name_rigid_center)
        # set the grafting sites, since the files have not explicitly have them
        self.graft_num = graft_index_positions.shape[0]

        if graft_index_positions is not None:
            if 0 in graft_index_positions:
                raise ValueError('cannot place grafters at center of nanocrystal')
            self.ff_reader = ff
            dict_nc = self.ff_reader.get_non_bonded('lj', atom_core, graft_atom, scale=True)
            dist = dict_nc['sigma']
            positions = self.position[graft_index_positions]
            vec = positions - self.position[np.newaxis, 0]
            nrm = la.norm(vec, axis=1)
            unt = vec/nrm[:, np.newaxis]
            self.graft_sites = positions + unt*dist
