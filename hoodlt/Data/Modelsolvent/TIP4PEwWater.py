"""
:module: TIP4P_Ew_Water
:platform: Unix. Windows
:synopsis: Implements a class defining TIP4P-Ew Water Molecule

.. moduleauthor:: Siray Zhu <sirayz@iastate.edu>
.. history:
"""

from __future__ import division
from hoodlt.Data.Modelsolvent.SolventAbs import SolventAbs
import numpy as np


class TIP4PEwWater(SolventAbs):
    """
    Defines solvent  TIP4P-Ew Water (:math:`\\mbox{H}_2\\mbox{O}`)
    """

    def __init__(self, ff):
        """
        create the TIP4P-Ew Water molecule
        """

        super(TIP4PEwWater, self).__init__(1, ff, 5, 'TIP4PEwWater')  # always only 1 repeat

        self.mass[1] = self.ff_reader.get_molecular_weight('OW')
        self.mass[2:4] = self.ff_reader.get_molecular_weight('H')
        self.mass[4] = self.ff_reader.get_molecular_weight('M')
        self.mass[0] = np.sum(self.mass[1:])

        self.charge[1] = self.ff_reader.get_charge('OW')
        self.charge[2:4] = self.ff_reader.get_charge('H')
        self.charge[4] = self.ff_reader.get_charge('M')

        # types
        self.types = ['_TIP4P_Ew_Water', 'OW', 'H', 'M']
        self.typeid = ['_TIP4P_Ew_Water'] + ['OW'] + ['H'] * 2 + ['M']

        # particle positions, in Angstroms
        self._set_positions()

        self.moment_inertia[0] = np.diag(self.moment_of_inertia())
        self.body = np.zeros(self.num_particles)

    def get_vector(self):
        """
        See documentation in BasicSystemEntity
        """

        return self.position[1] - self.position[0]  # points from O to the rigid center

    def _set_positions(self):
        """
        called by initializer to get positions

        :return: position array for the water molecule
        """
        self.position = np.zeros((5, 3))

        r_oh = self.ff_reader.get_bond_r0('OW-H')
        r_om = self.ff_reader.get_bond_r0('OW-M')

        theta_oho = self.ff_reader.get_angle_t0('H-OW-H')

        self.position[2] = [r_oh*np.cos(theta_oho/2), r_oh*np.sin(theta_oho/2), 0]
        self.position[3] = [r_oh*np.cos(theta_oho/2), -r_oh*np.sin(theta_oho/2), 0]
        self.position[4] = [r_om, 0, 0]

        self.shift(-1 * self._center_of_mass_ignoring_center())  # now the center of mass is at the origin
        self.position[0] = np.array([0.0, 0.0, 0.0])

    def _center_of_mass_ignoring_center(self):
        """
        Calculate the center of mass, ignoring the fictional rigid center.

        :return: a numpy array containing the position of the center of mass
        """

        r_cm = np.zeros(3)
        for i in range(len(self.mass) - 1):
            for j in range(3):
                r_cm[j] += self.mass[i + 1]*self.position[i + 1, j]

        return r_cm / np.sum(self.mass[1:])

