Installation Guide for Access ci
=====================================
First start by ssh into expanse you can find your username on https://allocations.access-ci.org/

ssh username@login.expanse.sdsc.edu

complete the set up in directory : /expanse/lustre/projects/ios116/username

Follow these steps to set up the environment and install necessary dependencies on the Expanse system.

.. note::
    Make sure you are set up on access-ci and if you run into any issues with expanse open a ticket they are very helpful.

Downloading Miniforge
----------------------

1. **Download and Install Miniforge**:

    Run the following commands to download and install Miniforge:
    ::

        curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh"
        bash Miniforge3-$(uname)-$(uname -m).sh

    .. note::
       During installation, you will be prompted to accept the license agreement and to select an installation folder. **Set the installation directory to** `/expanse/lustre/projects/ios116/username/miniforge3`, replacing `username` with your own username.

2. **Set Up Shell Initialization**:

    After the installation, you will be asked whether to initialize conda automatically for your shell. Choose **Yes** to make the setup easier.

Installing Conda Packages
-------------------------

1. **Install `conda-build` and `boa`**:

    Using the `mamba` installer, run:
    ::

        mamba install conda-build boa

Cloning the Repository
----------------------

1. **Clone the Software Repository**:

    Clone the `glotzerlab` software repository and navigate to the `conda` directory:
    ::

        git clone https://github.com/glotzerlab/software
        cd software/conda

Job Scripts for Download
========================

Expanse job script
------------------

To download the job script, click the link below:
after the first run comment out line 22 and uncomment out line 23

:download:`expanse.sh <_static/expanse.sh>`

Bridges job script 
-------------------

:download:`bridges.sh <_static/bridges.sh>` 

Installation Steps for HOODLT on access ci
====================================================

After setting up Miniforge and creating your environment, complete the following steps.

Configuring `.condarc`
----------------------

1. **Modify the `.condarc` file**:

   After installation, edit the `miniforge3/.condarc` file to set channel priority and specify custom channels:

   .. code-block:: yaml

       channel_priority: strict
       channels:
         - file:///expanse/lustre/projects/ios116/username/built_packages
         - conda-forge

       disallow:
         - openmpi
         - mpich
         - cuda-cudart-dev

   .. note::
      Ensure the `file://` prefix has three slashes (`///`) at the beginning of the `built_packages` path for correct configuration.

2. **Install Required Packages**:

   Verify that packages are installed from `built_packages` and not from `conda-forge`:

.. code-block:: bash

       mamba install hoomd
       mamba install mpi4py

Cloning and Installing HOODLT
-----------------------------

1. **Clone the Repository**:

   Download HOODLT from Bitbucket and switch to the desired branch for installation:
   
.. code-block:: bash

       git clone https://bitbucket.org/trvsst/hoodlt.git
       cd hoodlt
       git checkout v4
       pip install .

2. **Install Additional Dependencies**:

   Install other dependencies as needed:

.. code-block:: bash
    
    mamba install openpyxl
    mamba install importlib_resources
    mamba install gsd 
    mamba install paramed

Installing Foyer
-----------------
.. code-block:: bash

    mamba install foyer

Loading Required Modules
------------------------

Before launching jobs, load the necessary modules by running:

.. code-block:: bash

    module load gpu/0.15.4 openmpi/4.0.4 cuda12.2/toolkit/12.2.2




