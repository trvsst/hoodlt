#!/bin/bash

#SBATCH --job-name="glotzerlab-software build"
#SBATCH --output="build.out"
#SBATCH --account=dmr180023p
#SBATCH --partition=GPU-shared
#SBATCH --gpus=1
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=5
#SBATCH --mem=22750M
#SBATCH --export=ALL
#SBATCH --time=8:00:00

module purge
module restore
module load openmpi/5.0.3-gcc13.2.1 cuda/12.4.0

export SLURM_CPUS_PER_TASK=16

#./build.sh hoomd mpi4py --skip-existing --variants "{'cluster': ['bridges'], 'device': ['gpu'], 'gpu_platform': ['CUDA']}" --output-folder /ocean/projects/dmr180023p/pkakkar/built_packages
./build.sh mpi4py --skip-existing --variants "{'cluster': ['bridges'], 'device': ['gpu'], 'gpu_platform': ['CUDA']}" --output-folder /ocean/projects/dmr180023p/pkakkar/built_packages

