#!/bin/bash

#SBATCH --job-name="glotzerlab-software build"
#SBATCH --output="build.out"
#SBATCH --account=ios116
#SBATCH --partition=gpu-shared
#SBATCH --gpus=1
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=16
#SBATCH --mem-per-cpu=2000M
#SBATCH --export=ALL
#SBATCH --time=8:00:00

module purge
module restore
module load gpu/0.15.4
module load openmpi/4.0.4 cuda12.2/toolkit

export SLURM_CPUS_PER_TASK=16

./build.sh hoomd mpi4py --skip-existing --variants "{'cluster': ['expanse'], 'device': ['gpu'], 'gpu_platform': ['CUDA']}" --output-folder /expanse/lustre/scratch/pkakkar/built_packages
#./build.sh mpi4py --skip-existing --variants "{'cluster': ['expanse'], 'device': ['gpu'], 'gpu_platform': ['CUDA']}" --output-folder /expanse/lustre/scratch/ajuradoromero/temp_project/build_conda_channel
