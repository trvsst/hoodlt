.. _organizationandcollection:

Organization and Collection of Simulation Output Data
=====================================================

Introduction
------------

The purpose of these modules is to collect, organize, and scale relevant pieces of simulation output data.


CollectForceFieldData
---------------------

.. automodule:: hoodlt.Analysis.Collect.CollectForceFieldData
    :members:
    :undoc-members:
    :show-inheritance:

CollectGeneralData
------------------

.. automodule:: hoodlt.Analysis.Collect.CollectGeneralData
    :members:
    :undoc-members:
    :show-inheritance:

CollectGsdData
--------------

.. automodule:: hoodlt.Analysis.Collect.CollectGsdData
    :members:
    :undoc-members:
    :show-inheritance:

CollectHistData
---------------

.. automodule:: hoodlt.Analysis.Collect.CollectHistData
    :members:
    :undoc-members:
    :show-inheritance:

CollectLogData
--------------

.. automodule:: hoodlt.Analysis.Collect.CollectLogData
    :members:
    :undoc-members:
    :show-inheritance:

Representation of the Course of a Simulation
--------------------------------------------

.. automodule:: hoodlt.Analysis.Collect.Trajectory
    :members:
    :undoc-members:
    :show-inheritance:

ReInitHelper
------------
.. automodule:: hoodlt.Analysis.Collect.ReInitHelper
    :members:
    :undoc-members:
    :show-inheritance:
