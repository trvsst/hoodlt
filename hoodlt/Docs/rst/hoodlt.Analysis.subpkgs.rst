.. _AnalysisPackages:

Available Modules to use for Analysis
=====================================


Collection and Organization of Simulation Output Data
-----------------------------------------------------

.. toctree::

    hoodlt.Analysis.Collect

Calculation of Quantities from Newly Collected Output Data
----------------------------------------------------------

.. toctree::

    hoodlt.Analysis.Compute

Calculations Done on Individual Simulation Objects
--------------------------------------------------

.. toctree::

    hoodlt.Analysis.Analyze

Plotting of Calculation Results and Writing to Files
----------------------------------------------------

.. toctree::

    hoodlt.Analysis.Plot
