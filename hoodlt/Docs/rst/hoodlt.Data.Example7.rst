.. _NcsExample7:

Example 7: Perovskite Nanocube
==============================

Here we construct a single Perovskite nanocube system. This is similar to :ref:`NcsExample1`, the differences are:

#. The nanocubes are of cubic shapes.
#. There are two types of grafting sites and two types of ligands.
#. The size of the nanocore is changeable through parameter **l_size**.
#. The grafting density is changeable through parameter **ligand_position**.

.. code-block:: python

    import numpy as np
    from hoodlt.Data.Modelconfigurations.Saver import save_config
    from hoodlt.Data.Modelconfigurations.ConfigurationBuilder import ConfigurationBuilder
    from hoodlt.Data.Forcefield.ForceFieldReader import ForceFieldReader
    from hoodlt.Data.Modelnanoparticles.Perovskite import Perovskite
    from hoodlt.Data.Modelligands.OleateLigand import OleateLigand
    from hoodlt.Data.Modelligands.OleylammoniumLigand import OleylammoniumLigand

    # Initialize constants and objects
    ff = ForceFieldReader('opls_dry-ncs_mix')
    l_size = 2
    box_size = 100  # Box size for the simulation


    # Case 1 and 2: Build a single nanocube grafted with long ligands
    def build_nc(ligand_positions=None):
        builder = ConfigurationBuilder()
        core = Perovskite(ff, l_size=l_size, ligand_position=ligand_positions)
        lig1 = OleateLigand(ff, straight=True)
        lig2 = OleylammoniumLigand(ff, straight=True)
        ligs = [lig1] * core.graft_num[0] + [lig2] * core.graft_num[1]
        builder.add_nc(core, ligs, [0, 0, 0])
        builder.set_box(box_size)
        save_config(builder.conf)


    # Execute builds
    # Case 1: Full grafting
    build_nc()

    # Case 2: Partial grafting with specific ligand positions
    ligand_positions = [np.array([0]), np.array([0, 1])]
    build_nc(ligand_positions=ligand_positions)

**Expected Simulation Views:**

**Case 1**

.. raw:: html

    <link rel="stylesheet" href="_static/css/carousel.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <div id="case1Carousel" class="carousel slide" data-interval="false" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#case1Carousel" data-slide-to="0" class="active"></li>
        <li data-target="#case1Carousel" data-slide-to="1"></li>
        <li data-target="#case1Carousel" data-slide-to="2"></li>
        <li data-target="#case1Carousel" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="_static/perofront.png" alt="Front view">
          <div class="carousel-caption d-none d-md-block">
            <p>Front View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/peroleft.png" alt="Left view">
          <div class="carousel-caption d-none d-md-block">
            <p>Left View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/peroperspective.png" alt="Perspective view">
          <div class="carousel-caption d-none d-md-block">
            <p>Perspective View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/perotop.png" alt="Top view">
          <div class="carousel-caption d-none d-md-block">
            <p>Top View</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#case1Carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#case1Carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

**Case 2**

.. raw:: html

    <link rel="stylesheet" href="_static/css/carousel.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <div id="case2Carousel" class="carousel slide" data-interval="false" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#case2Carousel" data-slide-to="0" class="active"></li>
        <li data-target="#case2Carousel" data-slide-to="1"></li>
        <li data-target="#case2Carousel" data-slide-to="2"></li>
        <li data-target="#case2Carousel" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="_static/perofront2.png" alt="Front view">
          <div class="carousel-caption d-none d-md-block">
            <p>Front View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/peroleft2.png" alt="Left view">
          <div class="carousel-caption d-none d-md-block">
            <p>Left View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/peroperspective2.png" alt="Perspective view">
          <div class="carousel-caption d-none d-md-block">
            <p>Perspective View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/perotop2.png" alt="Top view">
          <div class="carousel-caption d-none d-md-block">
            <p>Top View</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#case2Carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#case2Carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>