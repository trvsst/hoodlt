.. _NcsExample8:

Example 8: Perovskite Nanocube Pair, Planar Square Lattice
==========================================================

Here we construct Perovskite nanocube pair systems and planar simple square lattice systems.

1. Building a nanocube pair is similar to building an NC pair as in :ref:`NcsExample2`.
2. Building a planar square lattice is similar to building a lattice as in :ref:`NCsExample4`.

.. code-block:: python

    from hoodlt.Data.Modelconfigurations.Saver import save_config
    from hoodlt.Data.Modelconfigurations.ConfigurationBuilder import ConfigurationBuilder
    from hoodlt.Data.Forcefield.ForceFieldReader import ForceFieldReader
    from hoodlt.Data.Modelnanoparticles.Perovskite import Perovskite
    from hoodlt.Data.Modelligands.OleateLigand import OleateLigand
    from hoodlt.Data.Modelligands.OleylammoniumLigand import OleylammoniumLigand
    from hoodlt.Lattices.Lat_dim2_1.ss_2d_lat_Mixt import LatSquare as Square
    from hoodlt.Data.ProcessConfigurations.Squeeze import Squeeze

    # Initialize configuration and forcefield
    ff = ForceFieldReader('opls_dry-ncs_mix')
    l_size = 2
    builder = ConfigurationBuilder()

    # Create nanocube pair
    core = Perovskite(forcefield=ff, l_size=l_size)
    lig1 = OleateLigand(ff, straight=True)
    lig2 = OleylammoniumLigand(ff, straight=True)
    ligands = [lig1]*core.graft_num[0] + [lig2]*core.graft_num[1]
    builder.add_nc(core, ligands, [-35, 0, 0])
    builder.add_nc(core, ligands, [35, 0, 0])
    builder.add_bond(0, 1, 'CTR-CTR1')
    builder.set_box(200)
    save_config(builder.conf)

    # Configure and build planar square lattice
    lat = Square(l_value=3, a_nn_e=50)
    lat.resize_box(3*50)
    builder.add_nc(core, ligands, [0, 0, 0])
    nc = builder.conf.particles[0]
    list_nc = Squeeze(ff, list_nc=[nc], list_radius=[23]).squeeze()

    # an empty ConfigurationBuilder object for the fcc lattice
    builder = ConfigurationBuilder()

    builder.build_lattice_from_reinit(list_nc, lat)

    # Set configuration parameters and save
    builder.set_alias('Pair & Lattice Simulation')
    save_config(builder.conf)

**Expected Simulation Views:**

**Nanocube Pair**

.. raw:: html

    <link rel="stylesheet" href="_static/css/carousel.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <div id="case1Carousel" class="carousel slide" data-interval="false" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#case1Carousel" data-slide-to="0" class="active"></li>
        <li data-target="#case1Carousel" data-slide-to="1"></li>
        <li data-target="#case1Carousel" data-slide-to="2"></li>
        <li data-target="#case1Carousel" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="_static/peronanofront.png" alt="Front view">
          <div class="carousel-caption d-none d-md-block">
            <p>Front View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/peronanoleft.png" alt="Left view">
          <div class="carousel-caption d-none d-md-block">
            <p>Left View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/peronanoperspective.png" alt="Perspective view">
          <div class="carousel-caption d-none d-md-block">
            <p>Perspective View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/peronanotop.png" alt="Top view">
          <div class="carousel-caption d-none d-md-block">
            <p>Top View</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#case1Carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#case1Carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>


**Planar Square Lattice**

.. raw:: html

    <link rel="stylesheet" href="_static/css/carousel.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <div id="case2Carousel" class="carousel slide" data-interval="false" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#case2Carousel" data-slide-to="0" class="active"></li>
        <li data-target="#case2Carousel" data-slide-to="1"></li>
        <li data-target="#case2Carousel" data-slide-to="2"></li>
        <li data-target="#case2Carousel" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="_static/peronanofront2.png" alt="Front view">
          <div class="carousel-caption d-none d-md-block">
            <p>Front View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/peronanoleft2.png" alt="Left view">
          <div class="carousel-caption d-none d-md-block">
            <p>Left View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/peronanoperspective2.png" alt="Perspective view">
          <div class="carousel-caption d-none d-md-block">
            <p>Perspective View</p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="_static/peronanotop2.png" alt="Top view">
          <div class="carousel-caption d-none d-md-block">
            <p>Top View</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#case2Carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#case2Carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>