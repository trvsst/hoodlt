.. _NcsData:

List of packages which define core, ligand, and solvent objects, respectively
=============================================================================

Subpackages
-----------

.. toctree::

    hoodlt.Data.Modelnanoparticles
    hoodlt.Data.Modelligands
    hoodlt.Data.Modelsolvents
    hoodlt.Data.Modelsubstrates
    hoodlt.Data.Modelconfigurations.CommonConfigurations
