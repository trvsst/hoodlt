.. _NanocrystalConfigs:

Classes that Facilitate Building Larger Configurations of Nanocrystals
======================================================================

Pair
----

.. automodule:: hoodlt.Data.Modelconfigurations.CommonConfigurations.PairConfigData
    :members:
    :undoc-members:
    :show-inheritance:

Symmetric Planar Configurations
-------------------------------

.. automodule:: hoodlt.Data.Modelconfigurations.CommonConfigurations.PlanarConfigData
    :members:
    :undoc-members:
    :show-inheritance:

Tetrahedron
-----------

.. automodule:: hoodlt.Data.Modelconfigurations.CommonConfigurations.TetraConfigData
    :members:
    :undoc-members:
    :show-inheritance:

Octahedron
----------

.. automodule:: hoodlt.Data.Modelconfigurations.CommonConfigurations.OctaConfigData
    :members:
    :undoc-members:
    :show-inheritance:

Cube
----

.. automodule:: hoodlt.Data.Modelconfigurations.CommonConfigurations.CubeConfigData
    :members:
    :undoc-members:
    :show-inheritance:

FCC Unit Cell
-------------

.. automodule:: hoodlt.Data.Modelconfigurations.CommonConfigurations.FccConfigData
    :members:
    :undoc-members:
    :show-inheritance:

Icosahedron
-----------

.. automodule:: hoodlt.Data.Modelconfigurations.CommonConfigurations.IcosConfigData
    :members:
    :undoc-members:
    :show-inheritance:

2 Rings of NCs: One Above and One Below the X-Y Plane
-----------------------------------------------------

.. automodule:: hoodlt.Data.Modelconfigurations.CommonConfigurations.TwoRingConfigData
    :members:
    :undoc-members:
    :show-inheritance:

Dodecahedron
------------

.. automodule:: hoodlt.Data.Modelconfigurations.CommonConfigurations.DodecConfigData
    :members:
    :undoc-members:
    :show-inheritance:
