.. _ListLigands:

List of available Ligands
=========================

Hydrocarbon Ligand
------------------

.. automodule:: hoodlt.Data.Modelligands.HydrocarbonLigand
    :members:
    :undoc-members:
    :show-inheritance:

Ammonium Ligand
---------------
.. automodule:: hoodlt.Data.Modelligands.AmmoniumLigand
    :members:
    :undoc-members:
    :show-inheritance:

Olefin Ligand
-------------

.. automodule:: hoodlt.Data.Modelligands.OlefinLigand
    :members:
    :undoc-members:
    :show-inheritance:

Polymer Ligand
--------------

.. automodule:: hoodlt.Data.Modelligands.PolymerLigand
    :members:
    :undoc-members:
    :show-inheritance:

Alkyl Ammonium Ligand
---------------------

.. automodule:: hoodlt.Data.Modelligands.AlkylAmmoniumLigand
    :members:
    :undoc-members:
    :show-inheritance:

Alkyl Carboxylate Ligand
------------------------

.. automodule:: hoodlt.Data.Modelligands.AlkylCarboxylateLigand
    :members:
    :undoc-members:
    :show-inheritance:

Oleylammonium Ligand
--------------------

.. automodule:: hoodlt.Data.Modelligands.OleylammoniumLigand
    :members:
    :undoc-members:
    :show-inheritance:

Polymer Internal Hybridizer Ligand
----------------------------------

.. automodule:: hoodlt.Data.Modelligands.PolymerInternalHybLigand
    :members:
    :undoc-members:
    :show-inheritance:

Thiocyanate Ligand
------------------

.. automodule:: hoodlt.Data.Modelligands.ThiocyanateLigand
    :members:
    :undoc-members:
    :show-inheritance:
