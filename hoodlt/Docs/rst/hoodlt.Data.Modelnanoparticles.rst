.. _ListCores:

List of Available Cores
=======================

Truncated Octahedron with 140 Core Particles and 62 Grafting Sites
------------------------------------------------------------------

.. automodule:: hoodlt.Data.Modelnanoparticles.TO140
    :members:
    :undoc-members:
    :show-inheritance:

Truncated Octahedron with 201 Core Particles and 80 Grafting Sites
------------------------------------------------------------------

.. automodule:: hoodlt.Data.Modelnanoparticles.TO201
    :members:
    :undoc-members:
    :show-inheritance:

Truncated Octahedron with 1289 Core Particles and 258 Grafting Sites
--------------------------------------------------------------------

.. automodule:: hoodlt.Data.Modelnanoparticles.TO1289
    :members:
    :undoc-members:
    :show-inheritance:

Truncated Octahedron with 4032 Core Particles and 432 Grafting Sites
--------------------------------------------------------------------

.. automodule:: hoodlt.Data.Modelnanoparticles.TO4032
    :members:
    :undoc-members:
    :show-inheritance:

Truncated Octahedron with 2200 Core Particles and 750 Grafting Sites
--------------------------------------------------------------------

.. automodule:: hoodlt.Data.Modelnanoparticles.TO2200
    :members:
    :undoc-members:
    :show-inheritance:

Truncated Octahedron with 3050 Core Particles and 913 Grafting Sites
--------------------------------------------------------------------

.. automodule:: hoodlt.Data.Modelnanoparticles.TO3050
    :members:
    :undoc-members:
    :show-inheritance:

Truncated Octahedra of Many Sizes and a General Grafting Scheme
---------------------------------------------------------------

.. automodule:: hoodlt.Data.Modelnanoparticles.TO
    :members:
    :undoc-members:
    :show-inheritance:

Cube with 8 Gold atoms
----------------------

.. automodule:: hoodlt.Data.Modelnanoparticles.AuCub8
    :members:
    :undoc-members:
    :show-inheritance:

Spherical shell with variable radius
------------------------------------

.. automodule:: hoodlt.Data.Modelnanoparticles.Sphere
    :members:
    :undoc-members:
    :show-inheritance:

Initialization information for variable spherical shell cores
-------------------------------------------------------------

.. automodule:: hoodlt.Data.Modelnanoparticles.SphereInfo
    :members:
    :undoc-members:
    :show-inheritance:
