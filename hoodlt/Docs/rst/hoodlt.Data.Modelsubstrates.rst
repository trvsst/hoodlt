.. _ListSubstrates:

List of available Substrates
============================

Rectangular Substrate
---------------------

.. automodule:: hoodlt.Data.Modelsubstrates.SquareSubstrate
    :members:
    :undoc-members:
    :show-inheritance:
