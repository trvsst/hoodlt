Lattices with Cubic NCs
=======================


AlB2 Sphere and Cube
--------------------

.. automodule:: hoodlt.GeneralPacking.AlB2_sphere_cube
    :members:
    :undoc-members:
    :show-inheritance:


CaTiO3b Sphere and Cube
-----------------------

.. automodule:: hoodlt.GeneralPacking.CaTiO3b_sphere_cube
    :members:
    :undoc-members:
    :show-inheritance:

NaCl Sphere and Cube
--------------------

.. automodule:: hoodlt.GeneralPacking.NaCl_sphere_cube
    :members:
    :undoc-members:
    :show-inheritance:
