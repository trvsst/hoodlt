.. _SimulationPackages:

Available classes which run (or aid in running) simulations with HOOMD
======================================================================


General HOOMD Simulations
-------------------------
.. automodule:: hoodlt.HOOMD.HoomdSimulation
    :members:
    :undoc-members:
    :show-inheritance:

Simulations that have CTR-CTR bonds
-----------------------------------
.. automodule:: hoodlt.HOOMD.SimulationWithBonds
    :members:
    :undoc-members:
    :show-inheritance:

Simulation parameters object
----------------------------
.. automodule:: hoodlt.HOOMD.SimParameters
    :members:
    :undoc-members:
    :show-inheritance:

Helper which sets the HOOMD force coefficents
---------------------------------------------
.. automodule:: hoodlt.HOOMD.CoefficientSetter
    :members:
    :undoc-members:
    :show-inheritance:
