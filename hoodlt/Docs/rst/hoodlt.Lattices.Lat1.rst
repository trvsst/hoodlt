Simple Lattices
===============

A15
---

.. automodule:: hoodlt.Lattices.Lat1.A15_lat_Mixt
    :members:
    :undoc-members:
    :show-inheritance:

bcc
---

.. automodule:: hoodlt.Lattices.Lat1.bcc_lat_Mixt
    :members:
    :undoc-members:
    :show-inheritance:

diamond
-------

.. automodule:: hoodlt.Lattices.Lat1.diamond_lat_Mixt
    :members:
    :undoc-members:
    :show-inheritance:

fcc
---

.. automodule:: hoodlt.Lattices.Lat1.fcc_lat_Mixt
    :members:
    :undoc-members:
    :show-inheritance:

hcp
---

.. automodule:: hoodlt.Lattices.Lat1.hcp_lat_Mixt
    :members:
    :undoc-members:
    :show-inheritance:

sc
--

.. automodule:: hoodlt.Lattices.Lat1.sc_lat_Mixt
    :members:
    :undoc-members:
    :show-inheritance:

LatticeSimple(Generic lattice)
------------------------------

.. automodule:: hoodlt.Lattices.Lat1.LatticeSimple
    :members:
    :undoc-members:
    :show-inheritance:

