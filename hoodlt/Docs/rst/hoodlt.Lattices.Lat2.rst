Binary Lattices
===============


AlB2
----

.. automodule:: hoodlt.Lattices.Lat2.AlB2_lat
    :members:
    :undoc-members:
    :show-inheritance:

AuCu
----

.. automodule:: hoodlt.Lattices.Lat2.AuCu_lat
    :members:
    :undoc-members:
    :show-inheritance:

CaB6
----

.. automodule:: hoodlt.Lattices.Lat2.CaB6_lat
    :members:
    :undoc-members:
    :show-inheritance:

CaCu5
-----

.. automodule:: hoodlt.Lattices.Lat2.CaCu5_lat
    :members:
    :undoc-members:
    :show-inheritance:

CaF2
----

.. automodule:: hoodlt.Lattices.Lat2.CaF2_lat
    :members:
    :undoc-members:
    :show-inheritance:

CaTiO3b
-------

.. automodule:: hoodlt.Lattices.Lat2.CaTiO3b_lat
    :members:
    :undoc-members:
    :show-inheritance:

Cr3Si
-----

.. automodule:: hoodlt.Lattices.Lat2.Cr3Si_lat
    :members:
    :undoc-members:
    :show-inheritance:

CsCl
----

.. automodule:: hoodlt.Lattices.Lat2.CsCl_lat
    :members:
    :undoc-members:
    :show-inheritance:

Cu3Au
-----

.. automodule:: hoodlt.Lattices.Lat2.Cu3Au_lat
    :members:
    :undoc-members:
    :show-inheritance:

DDQC_AT
-------

.. automodule:: hoodlt.Lattices.Lat2.DDQC_AT_lat
    :members:
    :undoc-members:
    :show-inheritance:

Fe4C
----

.. automodule:: hoodlt.Lattices.Lat2.Fe4C_lat
    :members:
    :undoc-members:
    :show-inheritance:

Li3Bi
-----

.. automodule:: hoodlt.Lattices.Lat2.Li3Bi_lat
    :members:
    :undoc-members:
    :show-inheritance:

MgCu2
-----

.. automodule:: hoodlt.Lattices.Lat2.MgCu2_lat
    :members:
    :undoc-members:
    :show-inheritance:

MgZn2
-----

.. automodule:: hoodlt.Lattices.Lat2.MgZn2_lat
    :members:
    :undoc-members:
    :show-inheritance:

NaCl
----

.. automodule:: hoodlt.Lattices.Lat2.NaCl_lat
    :members:
    :undoc-members:
    :show-inheritance:

NaZn13
------

.. automodule:: hoodlt.Lattices.Lat2.NaZn13_lat
    :members:
    :undoc-members:
    :show-inheritance:

Pt3O4
-----

.. automodule:: hoodlt.Lattices.Lat2.Pt3O4_lat
    :members:
    :undoc-members:
    :show-inheritance:

ReO3
----

.. automodule:: hoodlt.Lattices.Lat2.ReO3_lat
    :members:
    :undoc-members:
    :show-inheritance:

Th3P4
-----

.. automodule:: hoodlt.Lattices.Lat2.Th3P4_lat
    :members:
    :undoc-members:
    :show-inheritance:


ZnO
---

.. automodule:: hoodlt.Lattices.Lat2.ZnO_lat
    :members:
    :undoc-members:
    :show-inheritance:

ZnS
---

.. automodule:: hoodlt.Lattices.Lat2.ZnS_lat
    :members:
    :undoc-members:
    :show-inheritance:

bccAB6
------

.. automodule:: hoodlt.Lattices.Lat2.bccAB6_lat
    :members:
    :undoc-members:
    :show-inheritance:

cub_AB13
--------

.. automodule:: hoodlt.Lattices.Lat2.cub_AB13_lat
    :members:
    :undoc-members:
    :show-inheritance:

cub_fccAB13
-----------

.. automodule:: hoodlt.Lattices.Lat2.cub_fccAB13_lat
    :members:
    :undoc-members:
    :show-inheritance:

LatticeBinary(Generic class)
----------------------------

.. automodule:: hoodlt.Lattices.Lat2.LatticeBinary
    :members:
    :undoc-members:
    :show-inheritance:

