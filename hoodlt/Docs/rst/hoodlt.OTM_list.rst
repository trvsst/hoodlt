OTM Lattices
============


OTMobs
------

.. automodule:: hoodlt.OTM.OTMobs
    :members:
    :undoc-members:
    :show-inheritance:


AlB2_OTM
--------

.. automodule:: hoodlt.OTM.AlB2_OTM
    :members:
    :undoc-members:
    :show-inheritance:

AuCu_OTM
--------

.. automodule:: hoodlt.OTM.AuCu_OTM
    :members:
    :undoc-members:
    :show-inheritance:

CaB6_OTM
--------

.. automodule:: hoodlt.OTM.CaB6_OTM
    :members:
    :undoc-members:
    :show-inheritance:

CaCu5_OTM
---------

.. automodule:: hoodlt.OTM.CaCu5_OTM
    :members:
    :undoc-members:
    :show-inheritance:

CsCl_OTM
--------

.. automodule:: hoodlt.OTM.CsCl_OTM
    :members:
    :undoc-members:
    :show-inheritance:

Cu3Au_OTM
---------

.. automodule:: hoodlt.OTM.Cu3Au_OTM
    :members:
    :undoc-members:
    :show-inheritance:

CubAB13_OTM
-----------

.. automodule:: hoodlt.OTM.CubAB13_OTM
    :members:
    :undoc-members:
    :show-inheritance:

CaTiO3b_OTM
-----------

.. automodule:: hoodlt.OTM.CaTiO3b_OTM
    :members:
    :undoc-members:
    :show-inheritance:

DDQC_AT_OTM
-----------

.. automodule:: hoodlt.OTM.DDQC_AT_OTM
    :members:
    :undoc-members:
    :show-inheritance:

Fe4C_OTM
--------

.. automodule:: hoodlt.OTM.Fe4C_OTM
    :members:
    :undoc-members:
    :show-inheritance:

Li3Bi_OTM
---------

.. automodule:: hoodlt.OTM.Li3Bi_OTM
    :members:
    :undoc-members:
    :show-inheritance:

MgZn2_OTM
---------

.. automodule:: hoodlt.OTM.MgZn2_OTM
    :members:
    :undoc-members:
    :show-inheritance:

NaZn13_OTM
----------

.. automodule:: hoodlt.OTM.NaZn13_OTM
    :members:
    :undoc-members:
    :show-inheritance:

NaCl_OTM
--------

.. automodule:: hoodlt.OTM.NaCl_OTM
    :members:
    :undoc-members:
    :show-inheritance:

Th3P4_OTM
---------

.. automodule:: hoodlt.OTM.Th3P4_OTM
    :members:
    :undoc-members:
    :show-inheritance:
