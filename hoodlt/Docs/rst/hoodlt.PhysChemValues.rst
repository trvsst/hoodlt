Compendium of Formulas for Nanocrystals
=======================================

Contains different formulas used to compute NC properties.

Functions
---------

.. toctree::

    hoodlt.PhysChemValues_list.rst
