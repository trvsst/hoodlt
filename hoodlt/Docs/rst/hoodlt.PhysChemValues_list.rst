Compendium of Formulas for Nanocrystals
=======================================


OPM
---

.. automodule:: hoodlt.PhysChemValues.OPM
    :members:
    :undoc-members:
    :show-inheritance:


OCM
---

.. automodule:: hoodlt.PhysChemValues.OCM
    :members:
    :undoc-members:
    :show-inheritance:

OXM
---

.. automodule:: hoodlt.PhysChemValues.OXM
    :members:
    :undoc-members:
    :show-inheritance:

HydroCarbon
-----------

.. automodule:: hoodlt.PhysChemValues.HydroCarbon
    :members:
    :undoc-members:
    :show-inheritance:

Phys2lambxi
-----------

.. automodule:: hoodlt.PhysChemValues.Phys2lambxi
    :members:
    :undoc-members:
    :show-inheritance:



