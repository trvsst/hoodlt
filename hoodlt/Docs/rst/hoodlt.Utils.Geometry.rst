Geometric tools
===============

Curvature
---------

.. automodule:: hoodlt.Utils.Curvature
    :members:
    :undoc-members:
    :show-inheritance:

Polyhedra_Geometry
------------------

.. automodule:: hoodlt.Utils.Polyhedra_Geometry
    :members:
    :undoc-members:
    :show-inheritance:
