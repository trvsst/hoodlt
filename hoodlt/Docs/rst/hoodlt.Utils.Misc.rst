Miscelaneous functions used in applications
===========================================

Prime_Factors
-------------

.. automodule:: hoodlt.Utils.Prime_Factors
    :members:
    :undoc-members:
    :show-inheritance:
