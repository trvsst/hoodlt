Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Guides

   getting-started/index
   common-tasks/index
   tutorials/index
   notes/index

Core Modules
============

.. toctree::
   :maxdepth: 2
   :caption: Core Modules

   hoodlt.D_matrix
   hoodlt.Data
   hoodlt.HOOMD
   hoodlt.Analysis
   hoodlt.Lattices
   hoodlt.MonteCarlo
   hoodlt.OTM
   hoodlt.GeneralPacking
   hoodlt.PhysChemValues
   hoodlt.Potentials
   hoodlt.Utils
   hoodlt.Groups
   hoodlt.UnitTest

Common Tasks
============

.. toctree::
   :maxdepth: 2
   :caption: Common Tasks

   hoodlt.common_run_simulation
   hoodlt.common_packing_plots
   hoodlt.common_build_structure
   hoodlt.common_lattice_types
   hoodlt.common_predict_latticeconstant
   hoodlt.common_config_organization
   hoodlt.common_units
   hoodlt.common_run_cluster

Data Examples
=============

.. toctree::
   :maxdepth: 2
   :caption: Data Examples

   hoodlt.Data.Example1
   hoodlt.Data.Example2
   hoodlt.Data.Example3
   hoodlt.Data.Example4
   hoodlt.Data.Example5
   hoodlt.Data.Example6
   hoodlt.Data.Example7
   hoodlt.Data.Example8

Simulation Examples
===================

.. toctree::
   :maxdepth: 2
   :caption: Simulation Examples

   hoodlt.HOOMD.Example1
   hoodlt.HOOMD.Example2
   hoodlt.HOOMD.Example3
   hoodlt.HOOMD.Example4
   hoodlt.HOOMD.Example5
   hoodlt.HOOMD.Example6
   hoodlt.HOOMD.Example7

Analysis Examples
=================

.. toctree::
   :maxdepth: 2
   :caption: Analysis Examples

   hoodlt.Analysis.Example1
   hoodlt.Analysis.Example2
   hoodlt.Analysis.Example3
   hoodlt.Analysis.Example4
   hoodlt.Analysis.Example5

Python API
==========

.. toctree::
   :maxdepth: 2
   :caption: Python API

   package-hoomd
   package-hpmc
   package-md

Reference
=========

.. toctree::
   :maxdepth: 2
   :caption: Reference

   documentation
   changes
   developers
   open-source

Indices and Tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`