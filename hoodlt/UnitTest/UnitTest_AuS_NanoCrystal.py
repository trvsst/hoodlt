"""
:module: UnitTest_NanoCrystal_Properties.py
:platform: Unix, Windows
:synopsis: Defines the unit test for defined NCs

.. moduleauthor:: Alex Travesset <trvsst@ameslab.gov>, June 2024
.. history:
"""

import numpy as np
import unittest
from hoodlt.Data.Modelnanoparticles.AuS import AuS
from hoodlt.Data.Modelnanoparticles.AuSs.AuS_info import AuSConfigInfo
from hoodlt.Data.Forcefield.ForceFieldReader import ForceFieldReader


class TestNC(unittest.TestCase):

    def setUp(self):
        forcefield = ForceFieldReader("ncs-in-solvent")
        self.core_b = AuS(forcefield, 201, 1)
        self.core_c1 = AuS(forcefield, 1072, 1)
        self.core_c2 = AuS(forcefield, 1072, 2)
        self.core_d = AuS(forcefield, 3984, 1)
        self.core_e = AuS(forcefield, 4033, 1)

    def test_size(self):

        eps = 1e-8
        self.assertTrue(np.abs(self.core_b.nc_diameter()-21.16205735207631) < eps)
        self.assertTrue(np.abs(self.core_c1.nc_diameter() - 35.417001495852304) < eps)
        self.assertTrue(np.abs(self.core_c2.nc_diameter()-35.28666059492744) < eps)
        self.assertTrue(np.abs(self.core_d.nc_diameter()-53.35048360104462) < eps)
        self.assertTrue(np.abs(self.core_e.nc_diameter()-53.542820488759304) < eps)

        lclass = AuSConfigInfo(1072, 2)
        self.assertTrue(np.abs(lclass.nc_geometry['nc diameter from area']-35.94555727912313) < eps)
        val1 = lclass.nc_geometry['nc area']
        val2 = self.core_c2.area()
        self.assertTrue(np.abs(val1-val2) < eps)


if __name__ == '__main__':
    unittest.main()
