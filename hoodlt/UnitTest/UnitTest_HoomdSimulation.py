"""
:module: UnitTest_HoomdSimulation.py
:platform: Unix, Windows
:synopsis: Defines the unit test for the HoomdSimulation class

.. moduleauthor:: Arnau Jurado Romero <arnau.jurado@upc.edu>
.. history:
..                Alex Travesset <trvsst@ameslab.gov>
..                  - minor fixes to comply with PEP rules
..                Arnau Jurado Romero <arnau.jurado@upc.edu> July 2024
..                  - Now a FF object is initialized before creating simulation
"""

from hoodlt.HOOMD.HoomdSimulation import HoomdSimulation
from hoodlt.Data.Forcefield.ForceFieldReader import ForceFieldReader
import unittest
import numpy as np
import os
import shutil
import copy as cp
import hoomd


class TestHoomdSimulation(unittest.TestCase):
    """
    What's currently tested:
        - Device definition
        - Electrostatic definitions
        - NonBonded interaction definition
        - Bonded interaction definitions
        - Thermostat and integration group definitions
    """

    def setUp(self):
        temp = 300
        name = 'Au201-PEOLigand-n12_cSingle_sSpceWater_ffOpls-Aa_uNmAmuKjMol'
        ff = ForceFieldReader('opls-aa')
        list_electrostatic = [64, 4, 0.8, 0]
        self.sim = HoomdSimulation(sysfile=name, rcut=2.8, ff=ff, cluster=False, temp_in_kelvin=temp,
                                   list_electrostatic=list_electrostatic)

    def test_DevicePositions(self):
        self.assertIsInstance(self.sim.sim_dev, hoomd.device.Device)

    def test_Electrostatics(self):
        self.assertAlmostEqual(self.sim.potentials['pppm1'].r_cut, 0.8)
        self.assertAlmostEqual(self.sim.potentials['pppm1'].alpha, 0.0)

    def test_NonBonded(self):
        """
        Since testing every single LJ pair would be too lenghty we will instead
        test only the total number of pairs and three specific ones
        """

        self.assertEqual(len(self.sim.potentials['pair_LJ'].params), 78)
         
        self.assertAlmostEqual(self.sim.potentials['pair_LJ'].params[('Au', 'CS2')]['epsilon'], 0.9442822544133719)
        self.assertAlmostEqual(self.sim.potentials['pair_LJ'].params[('Au', 'CS2')]['sigma'], 0.309507673572078)
        self.assertAlmostEqual(self.sim.potentials['pair_LJ'].r_cut[('Au', 'CS2')], 0.8666214860018183)
           
        self.assertAlmostEqual(self.sim.potentials['pair_LJ'].params[('HP', 'OP')]['epsilon'], 0.2711849803510063)
        self.assertAlmostEqual(self.sim.potentials['pair_LJ'].params[('HP', 'OP')]['sigma'], 0.2692582403567252)
        self.assertAlmostEqual(self.sim.potentials['pair_LJ'].r_cut[('HP', 'OP')], 0.7539230729988304)

        self.assertAlmostEqual(self.sim.potentials['pair_LJ'].params[('HP', 'OW')]['epsilon'], 0.28566924169395624)
        self.assertAlmostEqual(self.sim.potentials['pair_LJ'].params[('HP', 'OW')]['sigma'], 0.281336097932704)
        self.assertAlmostEqual(self.sim.potentials['pair_LJ'].r_cut[('HP', 'OW')], 0.7877410742115711)

    def test_Bonds(self):

        self.assertAlmostEqual(self.sim.potentials['bond'+'harmonic'].params['C2-OP']['k'], 267776.0)
        self.assertAlmostEqual(self.sim.potentials['bond'+'harmonic'].params['C2-OP']['r0'], 0.141)

        self.assertAlmostEqual(self.sim.potentials['bond'+'harmonic'].params['C2-C2']['k'], 224262.4)
        self.assertAlmostEqual(self.sim.potentials['bond'+'harmonic'].params['C2-C2']['r0'], 0.1529)

        self.assertAlmostEqual(self.sim.potentials['bond'+'harmonic'].params['C3-OP']['k'], 267776.0)
        self.assertAlmostEqual(self.sim.potentials['bond'+'harmonic'].params['C3-OP']['r0'], 0.141)

        self.assertAlmostEqual(self.sim.potentials['bond'+'harmonic'].params['C2-CS2']['k'], 224262.4)
        self.assertAlmostEqual(self.sim.potentials['bond'+'harmonic'].params['C2-CS2']['r0'], 0.1529)

        self.assertAlmostEqual(self.sim.potentials['bond'+'harmonic'].params['CS2-SD']['k'], 185769.6)
        self.assertAlmostEqual(self.sim.potentials['bond'+'harmonic'].params['CS2-SD']['r0'], 0.181)

    def test_Angles(self):
        """"
        Like with NonBonded, there are too many to test individually,
        so we will just test the length and some of them
        """

        self.assertEqual(len(self.sim.potentials['angle'+'harmonic'].params), 14)

        self.assertAlmostEqual(self.sim.potentials['angle'+'harmonic'].params['HP-C2-HP']['k'], 276.144)
        self.assertAlmostEqual(self.sim.potentials['angle'+'harmonic'].params['HP-C2-HP']['t0'], 1.88146490155556)

        self.assertAlmostEqual(self.sim.potentials['angle'+'harmonic'].params['C2-OP-C2']['k'], 501.22)
        self.assertAlmostEqual(self.sim.potentials['angle'+'harmonic'].params['C2-OP-C2']['t0'], 1.91113549833333)

        self.assertAlmostEqual(self.sim.potentials['angle'+'harmonic'].params['CS2-C2-HP']['k'], 313.26)
        self.assertAlmostEqual(self.sim.potentials['angle'+'harmonic'].params['CS2-C2-HP']['t0'], 1.932079449)

    def test_Dihedrals(self):
        """
        Like with NonBonded, there are too many to test individually,
        so we will just test the length and some of them
        """

        self.assertEqual(len(self.sim.potentials['dihedral'+'opls'].params), 13)

        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['C2-OP-C2-HP']['k1'], 0.0)
        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['C2-OP-C2-HP']['k2'], 0.0)
        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['C2-OP-C2-HP']['k3'], 3.1798)
        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['C2-OP-C2-HP']['k4'], 0.0)

        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['C3-OP-C2-C2']['k1'], 2.7196)
        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['C3-OP-C2-C2']['k2'], -1.046)
        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['C3-OP-C2-C2']['k3'], 2.80328)
        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['C3-OP-C2-C2']['k4'], 0.0)

        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['OP-C2-C2-OP']['k1'], -2.3012)
        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['OP-C2-C2-OP']['k2'], 0.0)
        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['OP-C2-C2-OP']['k3'], 0.0)
        self.assertAlmostEqual(self.sim.potentials['dihedral'+'opls'].params['OP-C2-C2-OP']['k4'], 0.0)

    def test_Int_Group(self):
        self.assertIsInstance(self.sim.int_group, hoomd.filter.Union)

    def test_Integrator(self):
        self.assertIsInstance(self.sim.integrator.methods[0], hoomd.md.methods.ConstantVolume)
        self.assertIsInstance(self.sim.integrator.methods[0].thermostat, hoomd.md.methods.thermostats.MTTK)
        self.assertAlmostEqual(self.sim.integrator.methods[0].thermostat.kT.value, 2.4943369788037435)
        self.assertAlmostEqual(self.sim.integrator.methods[0].thermostat.tau, 0.05090252877228016)

        self.assertAlmostEqual(self.sim.integrator.dt, 0.0005090252877228016)
        self.assertEqual(len(self.sim.integrator.forces), 0)
        self.assertEqual(len(self.sim.integrator.constraints), 1)
        self.assertIsInstance(self.sim.integrator.rigid, hoomd.md.constrain.Rigid)


if __name__ == '__main__':
    unittest.main()
