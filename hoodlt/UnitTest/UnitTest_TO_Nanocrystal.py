"""
:module: UnitTest_NanoCrystal_Properties.py
:platform: Unix, Windows
:synopsis: Defines the unit test for defined NCs

.. moduleauthor:: Alex Travesset <trvsst@ameslab.gov>, June 2024
.. history:
"""

import numpy as np
import numpy.linalg as la
import unittest
from hoodlt.Data.Modelnanoparticles.TO201 import TO201
from hoodlt.Data.Modelnanoparticles.TO import TO
from hoodlt.Data.Modelnanoparticles.TOs.TO_info import TOInfo
from hoodlt.Data.Forcefield.ForceFieldReader import ForceFieldReader


class TestNC(unittest.TestCase):

    def setUp(self):
        self.forcefield = ForceFieldReader("ncs-in-solvent")
        self.core_a = TO201(self.forcefield)

    def test_size(self):


        eps = 1e-8
        lclass = TOInfo(1072, 2, 'Au')
        self.assertTrue(np.abs(self.core_a.nc_diameter()-19.726845642647014) < eps)
        self.assertTrue(np.abs(lclass.nc_geometry['core area']-3125.5197522997732) < eps)

        mat = np.arange(1, 100, dtype=int)
        to_1072 = TO(self.forcefield, 1072, 2, graft_index_positions=mat, graft_atom='S')

        # checking that grafting sites are correctly placed
        pnts_core = to_1072.position[mat]
        pnts = to_1072.graft_sites
        dist = la.norm(pnts_core-pnts, axis=1)
        self.assertTrue(np.abs(np.max(dist)-np.min(dist)) < eps)


if __name__ == '__main__':
    unittest.main()
