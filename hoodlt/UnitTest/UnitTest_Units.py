"""
:module: UnitTest_Units.py
:platform: Unix, Windows
:synopsis: Defines the unit test for PhysicalUnits and the rest of unit systems

.. moduleauthor:: Alex Travesset <trvsst@ameslab.gov> July 2024
"""

import unittest
import numpy as np
from hoodlt.Data.Units.NmAmuKjMolUnits import NmAmuKjMolUnits


class TestUnits(unittest.TestCase):

    def setUp(self):
        self.unt = NmAmuKjMolUnits()

    def test_charge(self):
        e0 = 8.8541878128e-12
        conv = self.unt.charge_construction_to_simulation
        conv1 = self.unt.pc.kj_mol_to_joule

        # check charge conversion
        self.assertTrue(np.abs(self.unt.charge_construction_to_si/(conv*self.unt.charge_simulation_to_si)-1) < 1e-6)
        self.assertTrue(np.abs(self.unt.charge_simulation_to_si/np.sqrt(4*np.pi*e0*conv1*1e-9)-1) < 1e-6)


if __name__ == '__main__':
    unittest.main()
