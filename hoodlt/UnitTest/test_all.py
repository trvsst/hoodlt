"""
:module: test_all.py
:platform: Unix, Windows
:synopsis: Runs all the unit tests

.. moduleauthor:: Alex Travesset <trvsst@ameslab.gov>,  July 2024
.. history:
..
"""

import glob
import unittest

test_files = glob.glob('UnitTest_*.py')
module_strings = [test_file[0:len(test_file)-3] for test_file in test_files]
suites = [unittest.defaultTestLoader.loadTestsFromName(test_file) for test_file in module_strings]
test_suite = unittest.TestSuite(suites)
test_runner = unittest.TextTestRunner().run(test_suite)

# unit tests that need to be fixed
l_unit_tests = ['UnitTest_Sphere', 'UnitTest_A15_overrp']
print('the following unit tests have at least one function that needs to be fixed')
for tst in l_unit_tests:
    print(tst)
